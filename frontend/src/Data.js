import type { Upgrades } from "./Types.js";

import { List } from "immutable";
import axios from "axios";
import { mockInfo, random } from "./Utils.js";

const TIMEOUT_MS = 5000;
const TRIES = 5;

function getParam(key: string): string {
  const params = window.location.search.slice(1);
  let value = null;
  params.split("&").forEach(part => {
    const kv = part.split("=");
    if (kv[0] === key) value = kv[1];
  });
  return value;
}

function getUser(): ?string {
  return getParam("user");
}

function getServer(): ?string {
  return "http://" + (getParam("server") || window.location.host);
}

function getData(
  success: (data: GameState, finished: boolean) => void,
  fail: () => void
): void {
  axios.get(`${getServer()}/state/`, { params: { user: getUser() } }).then(
    result => {
      const upgrades = List(result.data.upgrades);
      if (lastProcessedId > result.data.info.lastProcessedId) {
        return;
      } else {
        lastProcessedId = result.data.info.lastProcessedId;
      }

      let finished = waitForID == null;
      if (waitForID) {
        const up = upgrades.find(u => u.id === waitForID);
        const level = up.level;
        if (level === waitForLevel) {
          finished = true;
          waitForID = null;
          waitForLevel = null;
        }
      }
      if (!finished) {
        if (now() - waitForStart > TIMEOUT_MS) {
          waitForTries -= 1;
          if (waitForTries < 0) {
            waitForID = null;
            waitForLevel = null;
          }
        }
      }
      success(
        {
          info: result.data.info,
          upgrades,
        },
        finished
      );
    },
    error => {
      console.log(error);
      lastProcessedId = -1;
      fail();
    }
  );
}

let lastProcessedId = -1;
let waitForID = null;
let waitForLevel = null;
let waitForStart = 0;
let waitForTries = 0;

function now(): number {
  return Number(new Date());
}

function buyUpgrade(id: string, newLevel: number): void {
  waitForID = id;
  waitForLevel = newLevel;
  waitForStart = now();
  waitForTries = TRIES;
  axios.post(
    `${getServer()}/upgrade/`,
    { user: getUser(), upgrade: id, newLevel, timestamp: now() },
    { headers: { "Content-Type": "application/json" } }
  );
}

export { getData, buyUpgrade };
