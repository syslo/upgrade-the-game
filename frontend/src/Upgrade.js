// @flow
import type { List, Set } from "immutable";
import type { Info, UpgradeData } from "./Types.js";

import React from "react";
import "./Upgrade.css";
import { ClipLoader } from "react-spinners";
import { formatstr } from "./Utils.js";
import nullthrows from "nullthrows";

type Event = { target: { value: string, checked?: boolean } };

type Props = {
  allDisabled: boolean,
  childrenUpgrades: List<string>,
  data: UpgradeData,
  info: Info,
  isLoading: boolean,
  getFeature: (id: string) => number,
  onBuy: (id: string, newLevel: number) => void,
};

class Upgrade extends React.PureComponent<Props> {
  _onBuy = () => {
    this.props.onBuy(this.props.data.id, this.props.data.level + 1);
  };

  _canBeLeveled = () => {
    return (
      this.props.data.maxLevel === 0 ||
      this.props.data.maxLevel > this.props.data.level
    );
  };

  _canBePurchased = () => {
    return this.props.data.price <= this.props.info.money;
  };

  _renderLeft = () => {
    const data = this.props.data;

    const tooltip = this.props.getFeature("UDescription") ? (
      <span className="tooltiptext">
        {formatstr(this.props.data.description, [this.props.data.value])}
        {this.props.childrenUpgrades.count() > 0 &&
        this.props.getFeature("UChildren") ? (
          <div>
            <br />
            Ďalšie vylepšenia: {this.props.childrenUpgrades.join(", ")}
          </div>
        ) : null}
      </span>
    ) : null;
    const nextValues = this.props.getFeature("UDescription2")
      ? data.nextValues
      : data.nextValues.map(value => "??");

    if (
      data.parent == "Combo" &&
      data.level > 0 &&
      this.props.getFeature("ComboFeedback")
    ) {
      return [
        <div className="level" key="level">
          {data.level ? "\u2713" : "\u274c"}{" "}
        </div>,
        <div className="tooltip" key="name">
          <div className="name">
            {data.name}: <span className="value">{data.value}</span>
          </div>
          <div className="next-values">{nextValues.join(", ")}</div>
          {tooltip}
        </div>,
      ];
    }

    switch (data.maxLevel) {
      case 0:
        return [
          <div className="level" key="level">
            {data.level}
          </div>,
          <div className="tooltip" key="name">
            <div className="name">
              {data.name}: <span className="value">{data.value}</span>
            </div>
            <div className="next-values">{nextValues.join(", ")}...</div>
            {tooltip}
          </div>,
        ];
      case 1:
        return [
          <div className="level" key="level">
            {data.level ? "\u2713" : "\u274c"}{" "}
          </div>,
          <div className="tooltip" key="name">
            <div className="name">{data.name}</div>
            {tooltip}
          </div>,
        ];
      default:
        return null;
    }
  };

  _renderRight = () => {
    if (this.props.isLoading) {
      return (
        <div className="center-spinner">
          <ClipLoader size={30} color="#fff" />
        </div>
      );
    }
    if (!this._canBeLeveled()) {
      return null;
    }
    let digits = 3;
    if (this.props.data.price > 999) digits = 6;
    if (this.props.data.price > 999999) digits = 9;
    if (this.props.data.price > 999999999) digits = 12;
    const enabled = this._canBePurchased() && !this.props.allDisabled;
    return (
      <button
        className={`buy buy-${digits}d ${
          enabled ? "buy-enabled" : "buy-disabled"
        }`}
        disabled={!enabled}
        onClick={this._onBuy}
      >
        $<span className="smallSpace" />
        {this.props.data.price}
      </button>
    );
  };

  render() {
    const data = this.props.data;
    if (
      data.parent &&
      data.parent !== "" &&
      !this.props.getFeature(data.parent)
    ) {
      return null;
    }

    let clazz = "upgrade";
    clazz +=
      data.level > 0
        ? this._canBeLeveled()
          ? " part-bought"
          : " full-bought"
        : " not-bought";
    clazz +=
      !this._canBePurchased() && this._canBeLeveled()
        ? " not-available"
        : " available";

    return (
      <div className={clazz}>
        <div className="left">{this._renderLeft()}</div>
        <div className="right">{this._renderRight()}</div>
      </div>
    );
  }
}

export default Upgrade;
