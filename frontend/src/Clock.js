// @flow

import React from "react";

type Props = {
  showHours: boolean,
  showMinutes: boolean,
  showSeconds: boolean,
  showInterval: boolean,
  lastClick: number,
};

type State = {
  date: Date,
};

function dig(n: number, d: number): string {
  const ad = d - String(n).length;
  return (
    Array(ad)
      .fill(0)
      .join("") + String(n)
  );
}

class Clock extends React.Component<Props, State> {
  timerID = null;

  constructor(props: Props) {
    super(props);
    this.state = {
      date: new Date(),
    };
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 100);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date(),
    });
  }

  render() {
    const date = this.state.date;
    const diff = (Number(date) - this.props.lastClick) / 1000;
    const { showHours, showMinutes, showSeconds, showInterval } = this.props;
    const str =
      (showHours ? date.getHours() : "") +
      (showMinutes ? `:${dig(date.getMinutes(), 2)}` : "") +
      (showSeconds ? `:${dig(date.getSeconds(), 2)}` : "");
    const int = showInterval ? Math.floor(diff).toFixed(0) : null;
    return (
      <div className="clock">
        {str}
        {int && <span className="clock-interval"> ({int})</span>}
      </div>
    );
  }
}

export default Clock;
