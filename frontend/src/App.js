import React from "react";
import ScreenContainer from "./ScreenContainer.js";

function App() {
  return <ScreenContainer />;
}

export default App;
