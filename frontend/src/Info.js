// @flow

import type { GameState, Upgrades, Info, Click } from "./Types.js";

import React from "react";
import Clock from "./Clock.js";
import nullthrows from "nullthrows";
import { List, Map } from "immutable";
import { isVisible, getFeature, getFeatureValue } from "./Utils.js";

type Event = { target: { value: string, checked?: boolean } };

type Props = {
  gameState: GameState,
  life: number,
  getFeature: (id: string) => number,
  onChangeTheme: (value: boolean) => void,
};

function digStyle(n: number, d: number) {
  const ad = d - String(n).length;
  return (
    <span>
      <span className="leading-zeros">
        {Array(ad > 0 ? ad : 0)
          .fill(0)
          .join("")}
      </span>
      {n}
    </span>
  );
}

function sum(a: Array<number>): number {
  let s = 0;
  a.forEach(v => {
    s += v;
  });
  return s;
}

class InfoScreen extends React.PureComponent<Props> {
  _info = (): Info => {
    return this.props.gameState.info;
  };

  _getFeature = (id: string): number => {
    return getFeature(this.props.gameState.upgrades, id);
  };

  _getFeatureValue = (id: string): number => {
    return getFeatureValue(this.props.gameState.upgrades, id);
  };

  _renderName() {
    let color = "white";
    if (this.props.life >= 10) color = "rgb(0,255,0)";
    else if (this.props.life <= 0) color = "rgb(255,0,0)";
    else color = `rgb(255, ${55 + this.props.life * 20}, 0)`;
    return (
      <div className="i-name" style={{ color: color }}>
        {this._info().name}
      </div>
    );
  }

  _renderTime() {
    const allClicks = this._info().allClicks;
    const lastClick = allClicks[allClicks.length - 1];

    return (
      <Clock
        showHours={this._getFeature("Hours") > 0}
        showMinutes={this._getFeature("Minutes") > 0}
        showSeconds={this._getFeature("Seconds") > 0}
        showInterval={this._getFeature("Interval") > 0}
        lastClick={lastClick ? lastClick.timestamp : Number(new Date())}
      />
    );
  }

  _renderMoney() {
    return (
      <div className="i-money">
        ${digStyle(this._info().money, this._getFeatureValue("Digits"))}
      </div>
    );
  }

  _renderOtherMoney() {
    if (this._getFeature("MoneyOthers") === 0) {
      return null;
    }
    return (
      <div className="i-other">
        {Map(this._info().moneyOthers)
          .filter((v, k) => k !== this._info().name)
          .map((v, k) => (
            <div className="i-wide-item" key={k}>
              <span>{k}:</span>
              <span className="i-money">
                $<span className="smallSpace" />
                {v}
              </span>
            </div>
          ))
          .toList()}
      </div>
    );
  }

  _renderClickStats() {
    if (this._getFeature("Stats") === 0) {
      return null;
    }
    const allClicks = this._info().allClicks;
    return (
      <div className="iu-stats">
        <br />
        Počet klikov: {allClicks.length}
        <br />
        Celkový hrubý zisk: {sum(allClicks.map(c => c.moneyGross))}
        <br />
        Celkový čistý zisk: {sum(allClicks.map(c => c.moneyNet))}
        <br />
        Celkový bonusový zisk: {sum(allClicks.map(c => c.moneyOther))}
        <br />
      </div>
    );
  }

  _renderUpgradeStats() {
    if (this._getFeature("Stats") === 0) {
      return null;
    }
    const upgrades = this.props.gameState.upgrades;
    const levels: List<number> = upgrades.map(u => u.level);
    const boughtUniqCnt = levels.count(l => l > 0);
    const boughtCnt = levels.reduce((a, b) => a + b, 0);
    const visible = upgrades.filter(u => isVisible(u, this._getFeature));
    const nonBoughtCnt = visible.count(u => u.level === 0);
    const nonDiscoveredCnt = upgrades.count() - visible.count();

    return (
      <div className="iu-stats">
        <strong>Počet kúpených vylepšení: {boughtCnt}</strong>
        <br />
        Počet rôznych kúpených vylepšení: {boughtUniqCnt}
        <br />
        Počet nekúpených objavených vylepšení: {nonBoughtCnt}
        <br />
        Počet neobjavených vylepšení: {nonDiscoveredCnt}
        <br />
      </div>
    );
  }

  _formatTimeDiff(t: number, pc: ?Click): string {
    const diff = pc ? t - pc.timestamp : 0;
    return (diff / 1000).toFixed(1) + "s";
  }

  _renderLastClicks() {
    if (this._getFeature("Clicks") === 0) {
      return null;
    }
    const allClicks = this._info().allClicks.reverse();
    return (
      <table className="iu-table">
        <tbody>
          <tr>
            <th className="l">Typ</th>
            <th className="r">Rýchlosť</th>
            <th className="r">Hrubý</th>
            <th className="r">Čistý</th>
            <th className="r">Bonus</th>
          </tr>
          {allClicks.slice(0, 10).map((click: Click, index) => {
            const side = this._getFeature("ClicksSide")
              ? click.button === "LEFT"
                ? "Ľavé"
                : "Pravé"
              : "Klik";
            const time = this._getFeature("ClicksTime")
              ? this._formatTimeDiff(click.timestamp, allClicks[index + 1])
              : "";
            const gross = this._getFeature("ClicksGross")
              ? click.moneyGross
              : "?";
            const net = this._getFeature("ClicksNet") ? click.moneyNet : "?";
            const other = this._getFeature("ClicksOther")
              ? click.moneyOther
              : "?";
            return (
              <tr key={index}>
                <td className="l">{side}</td>
                <td className="r">{time}</td>
                <td className="r i-money">
                  $<span className="smallSpace" />
                  {gross}
                </td>
                <td className="r i-money">
                  $<span className="smallSpace" />
                  {net}
                </td>
                <td className="r i-money">
                  $<span className="smallSpace" />
                  {other}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  render() {
    return (
      <div className="info">
        <div className="i-wide-item">
          {this._renderName()}
          <span />
        </div>
        <div className="i-wide-item">
          <span />
          {this._renderMoney()}
        </div>
        <br />
        {this._renderOtherMoney()}
        <br />
        {this._renderUpgradeStats()}
        {this._renderClickStats()}
        <br />

        {this._renderTime()}
        {this._renderLastClicks()}
      </div>
    );
  }
}

export default InfoScreen;
