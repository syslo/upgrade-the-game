// @flow

import type { GameState, Upgrades } from "./Types.js";

import React from "react";
import Upgrade from "./Upgrade.js";
import InfoScreen from "./Info.js";
import "./Screen.css";
import { List, Map } from "immutable";

type Props = {
  loading: ?string,
  life: number,
  gameState: GameState,
  getFeature: (id: string) => number,
  onClickUpgrade: (id: string, newLevel: number) => void,
  onChangeTheme: (value: boolean) => void,
};

class Screen extends React.PureComponent<Props> {
  _onClickUpgrade = (name: string, newLevel: number) => {
    this.props.onClickUpgrade(name, newLevel);
  };

  render() {
    let childrenMap: Map<string, List<string>> = Map();
    this.props.gameState.upgrades.forEach(u => {
      childrenMap = childrenMap.set(
        u.parent,
        (childrenMap.get(u.parent) || List()).push(u.name)
      );
    });

    const columns = ["UI", "GAME", "COMBO"];
    let sorted: Map<string, Upgrades> = Map();
    columns.forEach(type => {
      sorted = sorted.set(
        type,
        this.props.gameState.upgrades.filter(u => u.type === type)
      );
    });

    return (
      <div className="screen">
        <div className="icolumn">
          <InfoScreen
            gameState={this.props.gameState}
            life={this.props.life}
            getFeature={this.props.getFeature}
            onChangeTheme={this.props.onChangeTheme}
          />
        </div>
        {sorted
          .map((upgrades, type) => (
            <div className="ucolumn" key={type}>
              {upgrades.map(u => (
                <Upgrade
                  allDisabled={this.props.loading != null}
                  childrenUpgrades={childrenMap.get(u.id) || List()}
                  data={u}
                  info={this.props.gameState.info}
                  isLoading={this.props.loading === u.id}
                  key={u.id}
                  getFeature={this.props.getFeature}
                  onBuy={this.props.onClickUpgrade}
                />
              ))}
            </div>
          ))
          .toList()}
      </div>
    );
  }
}

export default Screen;
