// @flow

import type { Info, Upgrades, UpgradeData, UpgradeMap } from "./Types.js";

import { List, Map } from "immutable";
import Immutable from "immutable";
import nullthrows from "nullthrows";

let memoMS: Map<string, UpgradeData> = Map();

function random(n: number): number {
  return Math.floor(Math.random() * n);
}

function formatstr(str: string, args: Array<string | number>): string {
  args.forEach((value, index) => {
    str = str.replace("{" + index + "}", String(value));
  });
  return str;
}

function mockInfo(): Info {
  return {
    name: "<no-user>",
    money: 0,
    lastProcessedId: -1,
    moneyOthers: {},
    allClicks: [],
  };
}

var lastUpgrades: Upgrades = List();
var lastUpgradeMap: UpgradeMap = Map();
function _getUpgradeMap(upgrades: Upgrades): UpgradeMap {
  if (lastUpgrades !== upgrades) {
    lastUpgradeMap = Map(upgrades.map(u => [u.id, u]));
  }

  return lastUpgradeMap;
}

function getFeature(upgrades: Upgrades, id: string): number {
  const u = _getUpgradeMap(upgrades).get(id);
  return u ? u.level : 0;
}

function getFeatureValue(upgrades: Upgrades, id: string): number {
  const u = _getUpgradeMap(upgrades).get(id);
  let value = u ? String(u.value) : "0";
  if (value.endsWith("%")) value = value.slice(0, -1);
  return Number(value);
}

function isVisible(upgrade: UpgradeData, getFeature: (id: string) => number) {
  return (
    upgrade.parent == null ||
    upgrade.parent === "" ||
    getFeature(upgrade.parent) > 0
  );
}

export { random, mockInfo, formatstr, getFeature, getFeatureValue, isVisible };
