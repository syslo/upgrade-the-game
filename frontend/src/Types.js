// @flow

import type { List, Map } from "immutable";

export type UpgradeType = "UI" | "GAME" | "COMBO";

export type UpgradeData = {
  // unikatne id bez diakritiky
  id: string,
  // meno s diakritikou
  name: string,
  // UI | GAME | COMBO
  type: UpgradeType,
  // popis, ktory sa zobrazi, ked na upgrade ukazes mysou
  description: string,
  // id, ktory upgrade musi byt kupeny, aby bol tento viditelny
  parent: string,
  // popis, co sa ti zhruna odomkne kupenim tohto
  future: string,
  // 1 ak sa neda levelovat, 0 ak je nekonecno levelov,
  maxLevel: number,

  // 0 = nekupeny, 1 = kupeny, N = level ak mame viac nez 1 level
  level: number,
  // aktualna cena (najblizsi nekupeny level)
  price: number,
  // aktualna (uz kupena) hodnota upgradu, napr "5%"
  value: string,
  // najblisich niekolko hodnot, ktore budu moct kupit ak level-upnu. [] pre nelevelovatelne upgrady
  // (specifikacia neurcuje kolko, ale mozeme sa dohodnu ze 3)
  nextValues: Array<string>,
};

export type Click = {
  timestamp: number,
  button: "LEFT" | "RIGHT",
  moneyGross: number, // kolko sme zarobili tymto klikom, pred tym ako nam ukradli
  moneyNet: number, // kolko sme zarobili tymto klikom, po tom ako nam ukradli
  moneyOther: number, // kolko penazi zarobili pomimo
};

export type Info = {
  // meno družinky
  name: string,
  // aktualny pocet penazi
  money: number,
  // pocet penazi ostatnych druziniek
  moneyOthers: { [string]: number },
  // poslednych zopar klikov (napr. 10)
  allClicks: Array<Click>,
};

// Vieme si vypocitat nasledovne
export type ComputedInfo = {
  // aktualny cas
  time: number,
  // pocet cifier
  moneyDigits: number,
  // pocet kupenych upgradov (vratane levelupov)
  upgrades: number,
};

export type Upgrades = List<UpgradeData>;
export type UpgradeMap = Map<string, UpgradeData>;

export type GameState = {
  info: Info,
  upgrades: Upgrades,
};

export type ScreenState = {
  loading: ?string,
  life: number,
  darkTheme: boolean,
  gameState: GameState,
};
