// @flow

import type { Element } from "react";

import React, { Component, createRef } from "react";
import nullthrows from "nullthrows";

type Props = {
  variables: { [string]: string },
  children: Element<any>,
};

class Theme extends Component<Props> {
  node = createRef<any>();

  static Light: { [string]: string };
  static Dark: { [string]: string };

  componentDidMount() {
    this._updateCSSVariables();
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.variables !== prevProps.variables) {
      this._updateCSSVariables();
    }
  }

  _updateCSSVariables() {
    Object.entries(this.props.variables).forEach(([prop, value]) =>
      nullthrows(this.node.current).style.setProperty(prop, value)
    );
  }

  render() {
    const { children } = this.props;
    return <div ref={this.node}>{children}</div>;
  }
}

Theme.Light = {
  "--col-back": "#F8FFFF",
  "--col-row1": "#D4EFDF",
  "--col-row2": "#D6EAF8",
  "--col-row3": "#E8DAEF",
  "--col-input": "#DEE",
  "--col-itext": "#222",
  "--col-otext": "#444",
  "--col-error": "#F22",
  "--col-rtext": "#222",
  "--col-focus": "#AA8844",
};

Theme.Dark = {
  "--col-back": "#282c34",
  "--col-row1": "#52BE80",
  "--col-row2": "#5DADE2",
  "--col-row3": "#BB8FCE",
  "--col-input": "#7f879a", // "#636b7e",
  "--col-itext": "#282c34",
  "--col-otext": "#e5e3dd",
  "--col-error": "#c00",
  "--col-rtext": "#fff",
  "--col-focus": "#fff000",
};

export default Theme;
