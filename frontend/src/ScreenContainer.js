// @flow

import type { ScreenState, GameState } from "./Types.js";

import React from "react";
import Screen from "./Screen.js";
import { getData, buyUpgrade } from "./Data.js";
import { mockInfo, getFeature } from "./Utils.js";
import { List } from "immutable";

type Props = {};
type State = ScreenState;

class ScreenContainer extends React.Component<Props, State> {
  timerID = null;
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: null,
      life: 0,
      darkTheme: true,
      gameState: {
        info: mockInfo(),
        upgrades: List(),
      },
    };
    getData(this._update, this._fail);
  }

  componentDidMount() {
    this.timerID = setInterval(() => this._tick(), 200);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  _tick = () => {
    getData(this._update, this._fail);
  };

  _update = (gameState: GameState, finished: boolean) => {
    this.setState((prevState: State) => {
      return {
        gameState,
        loading: finished ? null : prevState.loading,
        life: finished ? 10 : prevState.life - 1,
      };
    });
  };

  _fail = () => {
    console.log("fail");
    this.setState({
      life: 0,
      gameState: { upgrades: this.state.gameState.upgrades, info: mockInfo() },
    });
  };

  _onClickUpgrade = (id: string, newLevel: number) => {
    this.setState((prevState: State) => {
      return { loading: id };
    });
    buyUpgrade(id, newLevel);
  };

  _onChangeTheme = (value: boolean) => {
    this.setState({ darkTheme: value });
  };

  _getFeature = (id: string): number => {
    return getFeature(this.state.gameState.upgrades, id);
  };

  render() {
    return (
      <Screen
        {...this.state}
        getFeature={this._getFeature}
        onClickUpgrade={this._onClickUpgrade}
        onChangeTheme={this._onChangeTheme}
      />
    );
  }
}

export default ScreenContainer;
