from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import PermissionDenied

import json
from datetime import datetime

from . import config
from .models import Event
from .state import state


# Create your views here.


def _authorize(user):
    if user not in config.USERS:
        raise PermissionDenied("Not a valid user code.")

def get_state(request):
    user = request.GET.get('user', None)
    _authorize(user)
    return JsonResponse(state.get_for(user), json_dumps_params={'indent': 4})

def log(request):
    limit = int(request.GET.get('limit', 100))
    events = Event.objects.order_by('-id')[:limit]
    return HttpResponse("\n".join((
        "%s %s % 12s (% 12s) % 8s %s %s" % (
            " " if e.success else "X",
            datetime.fromtimestamp(e.timestamp/1000.0),
            config.USERS[e.user], e.user,
            e.type, e.target, str(e.level or "")
        ) for e in events
    )), content_type='text/plain')


def clicker(request):
    user = request.GET.get('user', None)
    _authorize(user)
    return render(request, "utg/clicker.html", {"user": user})

def clicked(request):
    body = json.loads(request.body)
    _authorize(body['user'])
    event = Event(
        user = body['user'],
        timestamp = body['timestamp'],
        type = "CLICK",
        target = body['target']
    )
    state.process_event(event)
    return HttpResponse('', status=(200 if event.success else 400))

def upgrade(request):
    body = json.loads(request.body)
    _authorize(body['user'])
    event = Event(
        user = body['user'],
        timestamp = body['timestamp'],
        type = "UPGRADE",
        target = body['upgrade'],
        level = body['newLevel'],
    )
    result = state.process_event(event)
    return JsonResponse(
        result,
        json_dumps_params={'indent': 4},
        status=(200 if event.success else 400),
    )
