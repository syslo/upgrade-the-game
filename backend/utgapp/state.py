from threading import Lock
from collections import defaultdict

from . import config
from . import upgrades
from .models import Event


class UserState:

    def __init__(self):
        self.all_clicks = []
        self.money = 0
        self.levels = defaultdict(lambda: 0)
        self.lastFactory = -1
        self.combos = {}


class State:

    def __init__(self):
        self.count = 0
        self.initialized = False
        self.users = defaultdict(UserState)

        self._cached = None

        self.lock = Lock()

    def _materialize(self, user):
        user_state = self.users[user]
        return {
            "info": {
                "lastProcessedId": self.count,
                "name": config.USERS[user],
                "money": user_state.money,
                "moneyOthers": {
                    config.USERS[u]: self.users[u].money
                    for u in config.USERS if u != user
                },
                "allClicks": user_state.all_clicks,
            },
            "upgrades": [
                self._materialize_upgrade(user, u)
                for u in upgrades.instances
            ],
        }

    def _materialize_upgrade(self, user, upgrade):
        user_state = self.users[user]
        level = user_state.levels[upgrade.id]
        value = upgrade.format_value(level)
        next_values = [upgrade.format_value(level+i) for i in range(1,6)]
        if upgrade.parent == upgrades.Combo and level > 0:
            iteration, note = user_state.combos.get(upgrade.id, [0, 0])
            value = upgrade.format_value(iteration)
            next_values = ["N=" + str(iteration), upgrade.format_note(note)]

        return {
            "id": upgrade.id,
            "name": upgrade.name,
            "type": upgrade.type,
            "parent": upgrade.parent.id if upgrade.parent else None,
            "description": upgrade.description,
            "childDecription": upgrade.child_description,
            "level": level,
            "maxLevel": upgrade.max_level,
            "price": upgrade.next_price(level, self._get_upgrade_value(user, upgrades.Discount)),
            "value": value,
            "nextValues": next_values,
        }

    def _process_event_core(self, event):
        self.count += 1
        if (event.type == "CLICK"): self._process_click(event)
        if (event.type == "UPGRADE"): self._process_upgrade(event)
        if not event.success: return


    def _process_click(self, event):
        user_state = self.users[event.user]
        money_before = user_state.money

        if (user_state.all_clicks):
            if (event.timestamp < user_state.all_clicks[-1]['timestamp'] + config.CLICK_COOLDOWN_MS):
                event.success = False
                return

        if user_state.lastFactory > 0:
            seconds =  event.timestamp // 1000 - user_state.lastFactory // 1000
            user_state.money += seconds * self._get_upgrade_value(event.user, upgrades.Factory)
            user_state.lastFactory = event.timestamp

        original_target = event.target
        if (self._get_upgrade_level(event.user, upgrades.Switch)):
            if event.target == "LEFT": event.target = "RIGHT"
            else: event.target = "LEFT"

        run_time = 0
        if len(user_state.all_clicks):
            run_time = event.timestamp - user_state.all_clicks[-1]['timestamp']
        if run_time < config.TIME_BONUS_LIMIT*1000:
            bonus_time = (config.TIME_BONUS_LIMIT*1000 - run_time) // 1000
            user_state.money += self._get_upgrade_value(event.user, upgrades.TimeBonus) * bonus_time

        self._recompute_combo(event.user, event.target, run_time)

        money_gross = self._get_upgrade_value(event.user, upgrades.Strength)
        if (event.target == "LEFT"): money_gross += self._get_upgrade_value(event.user, upgrades.LStrength)
        if (event.target == "RIGHT"): money_gross += self._get_upgrade_value(event.user, upgrades.RStrength)
        combo = self._get_combo_multiplier(event.user)
        money_gross += int(combo*money_gross)
        money_net = money_gross

        for user in config.USERS:
            if user == event.user:
                continue
            stolen = int(money_gross * 0.01 *self._get_upgrade_value(user, upgrades.Steal))
            copied = int(self._get_upgrade_value(user, upgrades.Copy))
            self.users[user].money += stolen
            self.users[user].money += copied
            money_net -= stolen

        insurance = int((money_gross - money_net) * 0.01 * self._get_upgrade_value(event.user, upgrades.Insurance))
        money_net += insurance
        if money_net < 0: money_net = 0

        money_other = user_state.money - money_before
        user_state.money += money_net
        for user in config.USERS:
            digits = self._get_upgrade_value(user, upgrades.Digits)
            if self.users[user].money >= 10**digits:
                self.users[user].money =  10**digits-1
        user_state.all_clicks.append({
            "timestamp": event.timestamp,
            "button": event.target,
            "moneyGross": money_gross,
            "moneyNet": money_net,
            "moneyOther": money_other,
        })
        event.target = original_target

    def _recompute_combo(self, user, target, run_time):
        combos = self.users[user].combos
        for k in combos:
            upgrade = upgrades.instance_by_id[k]
            combos[k] = upgrade.streak_reduce(combos[k], target, run_time)

    # ked vratim 0, je to ziadne kombo
    def _get_combo_multiplier(self, user):
        combos = self.users[user].combos
        bonus = 1.0
        for k in combos:
            upgrade = upgrades.instance_by_id[k]
            bonus *= 1 + 0.01*upgrade.get_value(combos[k][0])
        return bonus-1.;

    def _process_upgrade(self, event):
        user_state = self.users[event.user]
        upgrade = upgrades.instance_by_id[event.target]
        if user_state.levels[event.target] + 1 != event.level:
            event.success = False
            return
        price = upgrade.next_price(event.level-1, self._get_upgrade_value(event.user, upgrades.Discount))
        if user_state.money < price:
            event.success = False
            return

        if upgrade.parent == upgrades.Combo:
            user_state.combos[upgrade.id] = [0, 0]

        if upgrade.id == upgrades.Factory.id:
            user_state.lastFactory = event.timestamp

        user_state.money -= price
        user_state.levels[event.target] += 1

    def _get_upgrade_value(self, user, uClass):
        return uClass.instance.get_value(self.users[user].levels[uClass.id])

    def _get_upgrade_level(self, user, uClass):
        return self.users[user].levels[uClass.id]

    ########################################

    def process_event(self, event):
        with self.lock:
            self._try_initialize()
            self._process_event_core(event)
            event.id = self.count
            self._rematerialize()
        event.save()
        return self.get_for(event.user)

    def get_for(self, user):
        if self._cached is None:
            with self.lock:
                self._try_initialize()
        return self._cached[user]

    def _try_initialize(self):
        if not self.initialized:
            for e in Event.objects.all():
                self._process_event_core(e)
            self._rematerialize()
            self.initialized = True

    def _rematerialize(self):
        self._cached = { user: self._materialize(user) for user in config.USERS }

state = State()
