from django.db import models

# Create your models here.

class Event(models.Model):
    id = models.IntegerField(primary_key=True)
    timestamp = models.BigIntegerField()
    user = models.CharField(
        max_length=16,
    )
    type = models.CharField(
        max_length=16,
    )
    target = models.CharField(
        max_length=16,
    )
    level = models.IntegerField(null=True)
    success = models.BooleanField(default=True)
