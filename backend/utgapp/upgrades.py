from . import config
from . import combos
from random import randint

instances = []
instance_by_id = {}

class MetaUpgrade(type):
    def __new__(cls, name, bases, dct):
        self = super().__new__(cls, name, bases, dct)
        self.id = name
        if 'name' in dct:
            i = self()
            self.instance = i
            instances.append(i)
            instance_by_id[i.id] = i
        return self


class Upgrade(metaclass=MetaUpgrade):
    parent = None
    child_description = ""

    def next_price(self, level, discount):
        price = self.base_price * (2**level)
        discount_price = int(price * discount * 0.01)
        return max(1, price - discount_price)

    def get_value(self, level):
        return 0

    def format_value(self, level):
        return self.get_value(level)

class UIUpgrade(Upgrade):
    type = "UI"
    #max_level = 1

class GameUpgrade(Upgrade):
    type = "GAME"
    #max_level = 0

class ComboUpgrade(Upgrade):
    type = "COMBO"
    max_level = 0

    def format_value(self, level):
        return "Bonus " + str(self.get_value(level)) + '%';

    # [n, helper value], "LEFT" | "RIGHT", milliesconds
    def streak_reduce(self, previous, target, run_time):
        raise Exception("not implemented")

class SideComboUpgrade(ComboUpgrade):
    base_price = 1000

    def format_note(self, note):
        return "Posledné stlačenie: %s" % (["Ľavé", "Pravé"][note])

    def streak_reduce(self, previous, target, run_time):
        n, t = previous
        digit = int(target == "RIGHT")
        sequence = self.get_combo_sequence()
        if (sequence[n % len(sequence)] == digit):
            return [n+1, digit]
        return [0, digit]


########################################

class UIStart(UIUpgrade):
    name = "UI"
    base_price = 20
    description = "Odomkne sa vám možnosť vylepšovať používateľské rozhranie."
    max_level = 1


class Digits(UIUpgrade):
    parent = UIStart
    name = "Počet cifier"
    base_price = 50
    description = "Počítadlo peňazí má {0} cifier."
    max_level = 0

    def get_value(self, level):
        return 2 + level

class Hours(UIUpgrade):
    parent = UIStart
    name = "Hodiny"
    base_price = 100
    description = "Zobrazuje sa vám, koľko je hodín."
    max_level = 1


class Minutes(UIUpgrade):
    parent = Hours
    name = "Minúty"
    base_price = 200
    description = "Zobrazuje sa vám, koľko je minút."
    max_level = 1


class Seconds(UIUpgrade):
    parent = Minutes
    name = "Sekundy"
    base_price = 500
    description = "Zobrazuje sa vám, koľko je sekúnd."
    max_level = 1


class Interval(UIUpgrade):
    parent = Seconds
    name = "Interval"
    base_price = 1000
    description = "Zobrazuje sa vám, koľko sekúnd ubehlo od posledného stlačenia. Medzi klikačom a vašim zariadením môže byť malý konštantný posun."
    max_level = 1



class Spy(UIUpgrade):
    parent = UIStart
    name = "Špión"
    base_price = 300
    description = "Odomkne sa vám možnosť interagovať s inými družinkami."
    max_level = 1


class MoneyOthers(UIUpgrade):
    parent = Spy
    name = "Ostatní"
    base_price = 500
    description = "Zobrazuje sa vám počet peňazí ostatných družiniek."
    max_level = 1


class Stats(UIUpgrade):
    parent = UIStart
    name = "Štatistiky"
    base_price = 100
    description = "Zobrazia sa vám základné štatistiky o vylepšeniach."
    max_level = 1


class Clicks(UIUpgrade):
    parent = Stats
    name = "Kliky"
    base_price = 250
    description = "Zobrazia sa vám základné štatistiky o vašich stlačeniach."
    max_level = 1


class ClicksTime(UIUpgrade):
    parent = Clicks
    name = "Rýchlosť"
    base_price = 500
    description = "Zobrazí sa vám rýchlosť vašich stlačení."
    max_level = 1


class ClicksGross(UIUpgrade):
    parent = Clicks
    name = "Hrubý zisk"
    base_price = 180
    description = "Zobrazí sa vám hrubý zisk vašich kliknutí. (Už so započítanými kombami, pred krádežmi.)"
    max_level = 1


class ClicksNet(UIUpgrade):
    parent = Clicks
    name = "Čistý zisk"
    base_price = 270
    description = "Zobrazí sa vám čistý zisk vašich kliknutí. (Po krádežiach a poistení.)"
    max_level = 1


class ClicksSide(UIUpgrade):
    parent = Clicks
    name = "Typ stlačenia"
    base_price = 340
    description = "Zobrazí sa vám, kedy ste stlačili ľavé alebo pravé tlačidlo."
    max_level = 1

class ClicksOther(UIUpgrade):
    parent = Clicks
    name = "Bonusy"
    base_price = 270
    description = "Zobrazí sa vám súčet, ktorý ste získali ku kliknutiu. (Za továreň a rýchlostný bonus.)"
    max_level = 1

class UDescription(UIUpgrade):
    parent = UIStart
    name = "Popisy"
    base_price = 200
    description = "Zobrazia sa vám popisy k vylepšeniam (stačí myšou prejsť nad vylepšenie)."
    max_level = 1


class UChildren(UIUpgrade):
    parent = UDescription
    name = "Náväznosti"
    base_price = 500
    description = "V popise vylepšenia sa vám zobrazí, ktoré vylepšenia sa odomknú po jeho kúpe."
    max_level = 1


class UDescription2(UIUpgrade):
    parent = UDescription
    name = "Plánovač"
    base_price = 500
    description = "Zobrazia sa vám informácie o efektívnosti ďalších nákupov v rámci vylepšení."
    max_level = 1


class Button(GameUpgrade):
    name = "Tlačidlá"
    base_price = 30
    description = "Odomkne sa vám možnosť vylepšovať tlačidlo."
    max_level = 1


class Strength(GameUpgrade):
    parent = Button
    name = "Sila"
    base_price = 20
    description = "Každé stlačenie vám zarobí {0} peňazí. Toto sa násobí combami."
    max_level = 0

    def get_value(self, level):
        return 5 * round((level+3)**2 / 5)

class LStrength(GameUpgrade):
    parent = Strength
    name = "Ľavá sila"
    base_price = 60
    description = "Každé stlačenie ľavého tlačidla vám zarobí o {0} viac peňazí. Toto sa násobí combami."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 10 + 20 * (level-1)

class RStrength(GameUpgrade):
    parent = Strength
    name = "Pravá sila"
    base_price = 40
    description = "Každé stlačenie pravého tlačidla vám zarobí o {0} viac peňazí. Toto sa násobí combami."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 20 + 20 * (level-1)

class NoLimit(GameUpgrade):
    parent = Button
    name = "Ešte rýchlejšie"
    base_price = 500
    description = "Odteraz sa už nemusíte obmedzovať. Behajte tak rýchlo ako len vládzete!"
    max_level = 1


class Copy(GameUpgrade):
    parent = Spy
    name = "Odkukávanie"
    base_price = 2000
    description = "Vždy keď niektorí z vašich súperov stlačia tlačidlo, získate {0} peňazí."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 10 + 10 * (level-1)

class Steal(GameUpgrade):
    parent = Copy
    name = "Krádež"
    base_price = 1000
    description = "Vždy keď niektorí z vašich súperov stlačia tlačidlo, ukradnete pre seba {0} z ich zárobku."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 1 + 1 * (level-1)

    def format_value(self, level):
        return str(self.get_value(level)) + '%';

class Discount(GameUpgrade):
    parent = Button
    name = "Zľava"
    base_price = 200
    description = "Kúpa vylepšení vás stojí o {0} menej."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 5 + 2 * (level-1)

    def format_value(self, level):
        return str(self.get_value(level)) + '%';

class Factory(GameUpgrade):
    parent = Button
    name = "Továreň"
    base_price = 1000
    description = "Každú sekundu zarobíte {0} peňazí. Peniaze z továrne sa vám pripisujú na účet pri každom vašom kliknutí."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 5 + 5 * (level-1)

class Switch(GameUpgrade):
    parent = RStrength
    name = "Zámena"
    base_price = 2000
    description = "Pravé a ľavé tlačidlo sa vám vymenia."
    max_level = 1


class TimeBonus(GameUpgrade):
    parent = NoLimit
    name = "Rýchlostný bonus"
    base_price = 100
    description = "Pri každom stlačení, ktoré bolo rýchlejšie ako %s sekúnd, dostanete {0} peňazí za každú nevyužitú sekundu." % config.TIME_BONUS_LIMIT
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 10 + 5 * (level-1)

class Insurance(GameUpgrade):
    parent = Spy
    name = "Poistenie"
    base_price = 2000
    description = "Pri stlačení tlačidla sa vám vráti {0} peňazí, ktoré vám ukradli ostatné družinky."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 10 + 5 * (level-1)

    def format_value(self, level):
        return str(self.get_value(level)) + '%';

class Combo(ComboUpgrade):
    name = "Combo"
    base_price = 100
    description = "Odomkne sa vám možnosť robiť combá. Každé combo vám pridá percentuálny bonus ku sile tlačidla. Combá sa navzájom násobia."
    max_level = 1

class ComboFeedback(ComboUpgrade):
    parent = ClicksTime
    name = "Spätná väzba"
    base_price = 500
    description = "Pri combách sa vám bude zobrazovať, ako dlho sa vám ich darí držať. Tiež sa vám bude po kúpe komba \"Príkazy\" ukazovať, ako rýchlo máte zabehnúť ďalšie stlačenie."
    max_level = 1

class Accuracy(ComboUpgrade):
    parent = Combo
    name = "Presnosť"
    base_price = 1000
    description = "Aby ste si udržali toto combo, musíte tlačidlo stláčať rovnako rýchlo (dovolená ochýlka +- 1 sekunda od prvého stlačenia v combe)."
    max_level = 1

    def format_note(self, note):
        return "Približne %.1lf sekúnd" % (note/1000.)

    def streak_reduce(self, previous, target, run_time):
        n, t = previous
        if abs(run_time - t) > 1000:
            return [0, run_time]
        return [n+1, t]

    def get_value(self, level):
        return 8 * level

class Faster(ComboUpgrade):
    parent = Combo
    name = "Zrýchlenie"
    base_price = 1000
    description = "Aby ste si udržali toto combo, musíte tlačidlo stláčať čoraz rýchlejšie. Ak bude stlačenie tlačidla pomalšie ako stlačenie pred ním, combo sa vám vynuluje."
    max_level = 1

    def format_note(self, note):
        return "Rýchlejšie ako %.1lf sekúnd" % (note/1000.)

    def streak_reduce(self, previous, target, run_time):
        n, t = previous
        return [(0, n+1)[run_time < t], run_time]

    def get_value(self, level):
        return 12 * level

class Slower(ComboUpgrade):
    parent = Combo
    name = "Spomalenie"
    base_price = 1000
    description = "Aby ste si udržali toto combo, musíte tlačidlo stláčať čoraz pomalšie. Ak bude stlačenie tlačidla rýchlejšie ako stlačenie pred ním, combo sa vám vynuluje."
    max_level = 1

    def format_note(self, note):
        return "Pomalšie ako %.1lf sekúnd" % (note/1000.)

    def streak_reduce(self, previous, target, run_time):
        n, t = previous
        return [(0, n+1)[run_time > t], run_time]

    def get_value(self, level):
        return 10 * level

class Simon(ComboUpgrade):
    parent = Combo
    name = "Príkazy"
    base_price = 1000
    description = "Aby ste si udržali toto combo, musíte tlačidlo stláčať takou rýchlosťou, ako vám určila Maršálka (dovolená odchýlka +- 1 sekunda). Po každom stlačení vám Maršálka vygeneruje nejaký počet sekúnd. O presne toľko sekúnd máte tlačidlo stlačiť znova. Ak sa tak nestane, combo sa vám vynuluje."
    max_level = 1

    def format_note(self, note):
        return "Maršálka vraví %d sekúnd" % (note//1000)

    def streak_reduce(self, previous, target, run_time):
        n, t = previous
        next = randint(4,12) + randint(4,12) + randint(4,12)
        return [(0, n+1)[abs(run_time - t) <= 1000], next * 1000]

    def get_value(self, level):
        return 13 * level

class LR(SideComboUpgrade):
    parent = Combo
    name = "Striedanie"

    description = "Aby ste si udržali toto combo, musíte stláčať nastriedačku ľavé a pravé tlačidlo. V okamihu, keď stlačíte dvakrát za sebou to isté tlačidlo, combo sa vám vynuluje."
    max_level = 1

    def get_combo_sequence(self):
        return combos.LR

    def get_value(self, level):
        return 1 * level

class Binary(SideComboUpgrade):
    parent = Combo
    name = "Binárny kamzík"
    description = "Aby ste si udržali toto combo, stláčanie ľavého a pravého tlačidla má tvoriť čísla zapísané v binárnej sústave (ľavé tlačidlo reprezentuje 0 a pravé reprezentuje 1). Zapíšte si všetky nezáporné celé čísla v binárnej sústave a zreťazte ich za seba, pričom najviac významné cifry sú naľavo. Dostanete nekonečný reťazec 0 a 1. Stláčanie ľavého (0) a pravého (1) tlačidla musí zodpovedať znakom tohto reťazca. Tento reťazec začína 011011..."
    max_level = 1

    def get_combo_sequence(self):
        return combos.BINARY

    def get_value(self, level):
        return 6 * level

class Prime(SideComboUpgrade):
    parent = Combo
    name = "Prvočísla"
    description = "Aby ste si udržali toto combo, stláčanie ľavého a pravého tlačidla musí zodpovedať prvočíselnosti prirodzených čísel (ľavé tlačidlo reprezentuje zložené číslo, pravé reprezentuje prvočíslo). Predstavte si, že postupne počítate od 1 do nekonečna. Vždy keď narazíte na zložené číslo, stlačíte ľavé tlačidlo, keď na prvočíslo, stlačíte pravé tlačidlo. Keď sa pomýlite, combo sa vynuluje. Postupnosť stlačení teda začína: ľavé, pravé, pravé, ľavé, pravé..."
    max_level = 1

    def get_combo_sequence(self):
        return combos.PRIMES

    def get_value(self, level):
        return 5 * level

class Oraculum(SideComboUpgrade):
    parent = Combo
    name = "Veštenie"
    description = "Aby ste si udržali toto combo, stláčanie ľavého a pravého tlačidla musí zodpovedať náhodnej postupnosti, ktorú si aplikácia vygenerovala. Táto postupnosť sa v priebehu hry nemení. Táto postupnosť je tajná a prístup k nej nemáte. V aplikácii však vidíte, aká je aktuálna úroveň vášho comba. Keď vaše stlačenie nebude zodpovedať tajnej postupnosti, combo sa vynuluje."
    max_level = 1

    def get_combo_sequence(self):
        return combos.SECRET_COMBO

    def get_value(self, level):
        return 10 * level

class Music(ComboUpgrade):
    parent = UIStart
    name = "Hudba"
    base_price = 1
    description = "Umožňuje vám zvoliť si hudbu na želanie."
    max_level = 0

    def get_value(self, level):
        return 0 if level == 0 else 1 + 1 * (level-1)

    def format_value(self, level):
        return str(self.get_value(level)) + '\u266A';
