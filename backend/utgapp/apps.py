from django.apps import AppConfig


class UtgappConfig(AppConfig):
    name = 'utgapp'
